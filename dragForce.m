function D = dragForce( Cd,ro, velocity, area )
Dvalue=Cd* (ro*norm(velocity)^2/2) * area;
D = (-velocity/norm(velocity)) * Dvalue;

end

