function h = rocketHeight(position, planetCenter, planetRange)
    r = norm(position - planetCenter);
    h = r - planetRange;
end

