function varargout = gui(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
clc;
global idd;
idd=1;
global dict;
dict = zeros(1000,10);
global atmosfereAltitude;
global numberOfRanges;
global ranges;
numberOfRanges = 10;
ranges = zeros(1,10);
atmosfereAltitude = 44000;

altitude = 0:100:atmosfereAltitude;
density = airDensity(altitude);
maxDensity = airDensity(0);
rangeSize = maxDensity/numberOfRanges;
rangeNo = 10;
for i=atmosfereAltitude:-100:0
    if airDensity(i) > (numberOfRanges-rangeNo) * rangeSize
        ranges(rangeNo) = i;
        rangeNo=rangeNo-1;
    end
end
axes(handles.wykresatmosfery);
plot(altitude, density);
xlabel('wysoko��[m]');
ylabel('g�sto��[kg/m^3');
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% atmosfera ladnie narysowana
% na poczatku lata sobie spokojnie potem cos zadzialuje sila i zaczyna
% spadac
% wieksza atmosfera zeby bylo cos widac
% rozgrzewajacy sie satelita strzalka albo zmiana koloru


function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
planetRange = str2double(get(handles.promien, 'String'));
loadMass = str2double(get(handles.masaladunku, 'String'));
fuelMass = str2double(get(handles.masapaliwa, 'String'));
planetMass = str2double(get(handles.masa, 'String'));
Cd = str2double(get(handles.opor, 'String'));
area = str2double(get(handles.powierzchnia, 'String'));
fuel = fuelMass;
position = [str2double(get(handles.positionx, 'String')), str2double(get(handles.positiony, 'String'))];
planetCenter = [0, 0];
velocity = [str2double(get(handles.velocityx, 'String')), str2double(get(handles.velocityy, 'String'))];

keepLooping = true;
i=1;
buffersize = str2double(get(handles.krokanimacji, 'String'));
buffer = zeros(buffersize, 2);
plot_atmosphere(planetCenter, planetRange);
hold on;
while keepLooping
    rocketMass = loadMass + fuelMass;
    G = gravityForce(position, planetCenter, rocketMass, planetMass);
    height = rocketHeight(position, planetCenter, planetRange);


    ro = airDensity(height);
    D = dragForce(Cd, ro, velocity, area);
    a = (G+D)/rocketMass;
    
    velocity = velocity + a;
    
    if height < 0
        velocity =[0 0];
        beep
    end
    
    position = position + velocity;
    buffer(mod(i,buffersize)+1,:) = position;
    if mod(i, buffersize) == 0
        plot_buffer(planetCenter, planetRange, buffer, D);
         telemetry(handles, height, velocity, a, fuel, D, i);
    end
    i=i+1;
end

function arrows(position, velocity, acceleration, planetRange)
v = (planetRange*velocity)/norm(velocity);
a = (planetRange*acceleration)/norm(acceleration);
quiver(position(1),position(2), v(1), v(2));
quiver(position(1),position(2), a(1), a(2));

function telemetry(handles, altitude, velocity, acceleration, fuel, D, i)
set(handles.wysokosc, 'String', altitude);
set(handles.szybkosc, 'String', norm(velocity));
set(handles.przyspieszenie, 'String', norm(acceleration));
set(handles.paliwo, 'String', fuel);
set(handles.silaoporu, 'String', norm(D));
set(handles.iteracja, 'String', i);

function plot_buffer(planetCenter, planetRange, buffer, D)
normd = norm(D)/1000;
if(normd > 1)
    normd = 1;
end
plot(buffer(:,1), buffer(:,2),'--', 'Color', [1 1-normd 1-normd]);
set(gca, 'Color', 'black');
axis equal;
pause(0.001);

function plot_atmosphere(planetCenter, planetRange)
global numberOfRanges;
global ranges;
for i=numberOfRanges:-1:1
otherColor = 0.8*(1/numberOfRanges)*i;
color = [otherColor, otherColor, 1];
leftDownX = planetCenter(1) - (planetRange+ranges(i));
leftDownY = planetCenter(2) - (planetRange+ranges(i));
position = [leftDownX, leftDownY,...
    2*(planetRange + ranges(i)),2*(planetRange + ranges(i))];
rectangle('Position',position,'Curvature',[1,1], 'FaceColor',color)
end
leftDownX = planetCenter(1) - planetRange;
leftDownY = planetCenter(2) - planetRange;
position = [leftDownX, leftDownY,...
    2*planetRange, 2*planetRange];
rectangle('Position', position, 'Curvature', [1,1], 'FaceColor','green')


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
global idd;
global dict;

values = str2double(get([handles.masaladunku, handles.masapaliwa, handles.velocityx, handles.velocityy, handles.positionx,...
    handles.positiony, handles.opor, handles.powierzchnia, handles.promien, handles.masa], 'String'));
temp = get(handles.listbox1, 'String');
set(handles.listbox1, 'String', [temp; idd]);
dict(idd, :) = values;

idd = idd+1;



function krokanimacji_Callback(hObject, eventdata, handles)
% hObject    handle to krokanimacji (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of krokanimacji as text
%        str2double(get(hObject,'String')) returns contents of krokanimacji as a double


% --- Executes during object creation, after setting all properties.
function krokanimacji_CreateFcn(hObject, eventdata, handles)
% hObject    handle to krokanimacji (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
