function F = gravityForce(rocketPosition, planetCenter, rocketMass, planetMass)
    d = rocketPosition - planetCenter;
    r = norm(d);
    F = (G*rocketMass*planetMass)/(r^2);
    e = d/norm(d);
    F=-e*F;
end
