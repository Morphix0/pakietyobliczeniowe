function v = escapeVelocity(planetMass, planetRange)
    v= sqrt(2*G*planetMass/planetRange);
end