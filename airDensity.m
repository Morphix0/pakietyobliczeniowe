function ro = airDensity( altititude )
global atmosfereAltitude;
if altititude <= atmosfereAltitude
    R=8.31447;
    M=0.0289644;
    p0=101325;
    T0 = 288.15;
    L=0.0065;
    g=9.806;
    T = T0 - L*altititude;
    x=(1- L*altititude/T0);
    p=p0*x.^((g*M)/(R*L));
    ro = (p*M)./(R*T);
else
    ro = 0;
end
end

